# Maze Game

A simple maze game made in Python for my first term university project.

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9989b873295c43f5b0d7c5bdb0ee3d02)](https://www.codacy.com/app/jm0804/mazegame?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=JM0804/mazegame&amp;utm_campaign=Badge_Grade)

## Licence

Maze Game is licensed under the GNU GPLv3.
