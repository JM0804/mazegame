import pygame, os

# Block size in pixels
BLOCK_SIZE = 30

# Window background colour
BACKGROUND_COLOUR = (0, 0, 0)

# Colour used by sprites to denote transparency
TRANSPARENCY_COLOUR_KEY = (255, 255, 255)

# Level file block type representations
BLOCKTYPE_NULL              = " "
BLOCKTYPE_WALL              = "W"
BLOCKTYPE_ELECTRIFIED_WALL  = "E"
BLOCKTYPE_SPAWN             = "S"
BLOCKTYPE_SOFTSPAWN         = "s"
BLOCKTYPE_EXIT              = "X"
BLOCKTYPE_ENEMY_HORIZONTAL  = "H"
BLOCKTYPE_ENEMY_VERTICAL    = "V"
BLOCKTYPE_CHECKPOINT        = "C"

# Get the number of levels
LEVEL_COUNT = len([name for name in os.listdir("levels/") if name.endswith(".lvl") and os.path.isfile(os.path.join("levels/", name))])

class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of the player"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_PLAYER
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE
        self.spawnX = x*BLOCK_SIZE
        self.spawnY = y*BLOCK_SIZE
        self.lives = 3
        self.speed = 70
        self.won = False

    def collidedWith(self, spriteList):
        """Renames the collide_rect() function and removes a parameter for ease of use"""
        return pygame.sprite.collide_rect(self, spriteList)

    def update(self, dt):
        """Moves player based on user input and checks for collisions"""
        keys = pygame.key.get_pressed()
        # For each directional key, if the player is within the bounds of the window,
        # let them move, but push them back if they collide with a wall
        if keys[pygame.K_LEFT]:
            if self.rect.left > 0:
                self.rect.left -= int(self.speed / dt)
                for wall in wall_list:
                    if self.collidedWith(wall):
                        if self.rect.left < wall.rect.right:
                            self.rect.left = wall.rect.right
        if keys[pygame.K_RIGHT]:
            if self.rect.right < windowSize[0]:
                self.rect.left += int(self.speed / dt)
                for wall in wall_list:
                    if self.collidedWith(wall):
                        if self.rect.right > wall.rect.left:
                            self.rect.right = wall.rect.left
        if keys[pygame.K_UP]:
            if self.rect.top > 0:
                self.rect.top -= int(self.speed / dt)
                for wall in wall_list:
                    if self.collidedWith(wall):
                        if self.rect.top < wall.rect.bottom:
                            self.rect.top = wall.rect.bottom
        if keys[pygame.K_DOWN]:
            if self.rect.bottom < windowSize[1]:
                self.rect.top += int(self.speed / dt)
                for wall in wall_list:
                    if self.collidedWith(wall):
                        if self.rect.bottom > wall.rect.top:
                            self.rect.bottom = wall.rect.top
        
        for electrifiedWall in electrifiedWall_list:
            if self.collidedWith(electrifiedWall):
                self.returnToSpawn()

        for enemy in enemy_list:
            if self.collidedWith(enemy):
                self.returnToSpawn()

        for exitpoint in exitpoint_list:
            if self.collidedWith(exitpoint):
                self.won = True
        
        for checkpoint in checkpoint_list:
            if self.collidedWith(checkpoint):
                # Set the players spawn point to the position they were on when they touched the checkpoint
                self.spawnX = self.rect.x
                self.spawnY = self.rect.y
                # Display checkpoint label in the centre of the screen
                checkpointLabel = checkpointFont.render("Checkpoint reached!", 1, (255, 255, 255))
                labelWidth = checkpointLabel.get_width()
                labelHeight = checkpointLabel.get_height()
                window.blit(checkpointLabel, (windowSize[0]/2-labelWidth/2, windowSize[1]/2-labelHeight/2))

    def returnToSpawn(self):
        """Sends the player back to the spawn point and takes away a life"""
        self.rect.x = self.spawnX
        self.rect.y = self.spawnY
        self.lives -= 1


class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, y, horizontal):
        """Creates an instance of an enemy"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_ENEMY
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE
        self.speed = 100
        self.horizontal = horizontal
        self.flip = False

    def update(self, dt):
        """Moves enemy horizontally or vertically within the bounds of the window"""
        if self.horizontal:
            if self.flip:
                if self.rect.left < 0 or self.rect.right < windowSize[0]:
                    self.rect.left += int(self.speed / dt)
                else:
                    self.flip = not self.flip
            else:
                if self.rect.left > 0 or self.rect.right > windowSize[0]:
                    self.rect.left -= int(self.speed / dt)
                else:
                    self.flip = not self.flip
        else:
            if self.flip:
                if self.rect.top < 0 or self.rect.bottom < windowSize[1]:
                    self.rect.top += int(self.speed / dt)
                else:
                    self.flip = not self.flip
            else:
                if self.rect.top > 0 or self.rect.bottom > windowSize[1]:
                    self.rect.top -= int(self.speed / dt)
                else:
                    self.flip = not self.flip


class Wall(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of a wall"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_WALL
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE


class ElectrifiedWall(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of a wall"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_ELECTRIFIED_WALL
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE


class SpawnPoint(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of a spawn point"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_SPAWNPOINT
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE


class SoftSpawn(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of a soft spawn point (these are just for show and don't act as real spawn points)"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_SPAWNPOINT
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE


class ExitPoint(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of an exit point"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_EXITPOINT
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE


class CheckPoint(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """Creates an instance of a checkpoint"""
        pygame.sprite.Sprite.__init__(self)
        self.image = IMAGE_CHECKPOINT
        self.image.set_colorkey(TRANSPARENCY_COLOUR_KEY)
        self.rect = self.image.get_rect()
        self.rect.x = x*BLOCK_SIZE
        self.rect.y = y*BLOCK_SIZE


def loadLevel(number):
    """Load the level file and render it"""
    # Clear sprite groups from last level
    wall_list.empty()
    electrifiedWall_list.empty()
    softspawn_list.empty()
    spawnpoint_list.empty()
    exitpoint_list.empty()
    checkpoint_list.empty()
    player_list.empty()
    enemy_list.empty()
    
    # Display loading text
    displayCentreText("Loading...")
    pygame.display.flip()
    
    # Load the level
    level = open("levels/" + str(number) + ".lvl", "r")
    lines = level.readlines()
    xBlocks = len(lines[0]) - 1
    yBlocks = len(lines)
    
    # Render blocks
    for y in range(yBlocks):
        for x in range(xBlocks):
            if lines[y][x] == BLOCKTYPE_WALL:
                # Render wall
                wall = Wall(x, y)
                wall_list.add(wall)
            elif lines[y][x] == BLOCKTYPE_ELECTRIFIED_WALL:
                # Render electrified wall
                electrifiedWall = ElectrifiedWall(x, y)
                electrifiedWall_list.add(electrifiedWall)
            elif lines[y][x] == BLOCKTYPE_SPAWN:
                # Render player and spawn point
                player = Player(x, y)
                player_list.add(player)
                spawn = SpawnPoint(x, y)
                spawnpoint_list.add(spawn)
            elif lines[y][x] == BLOCKTYPE_SOFTSPAWN:
                # Render soft spawn point
                softspawn = SoftSpawn(x, y)
                softspawn_list.add(softspawn)
            elif lines[y][x] == BLOCKTYPE_EXIT:
                # Render exit point
                exitpoint = ExitPoint(x, y)
                exitpoint_list.add(exitpoint)
            elif lines[y][x] == BLOCKTYPE_ENEMY_HORIZONTAL:
                # Render horizontal enemy with wall behind it
                wall = Wall(x, y)
                wall_list.add(wall)
                enemy = Enemy(x, y, True)
                enemy_list.add(enemy)
            elif lines[y][x] == BLOCKTYPE_ENEMY_VERTICAL:
                # Render vertical enemy with wall behind it
                wall = Wall(x, y)
                wall_list.add(wall)
                enemy = Enemy(x, y, False)
                enemy_list.add(enemy)
            elif lines[y][x] == BLOCKTYPE_CHECKPOINT:
                # Render checkpoint
                checkpoint = CheckPoint(x, y)
                checkpoint_list.add(checkpoint)
            
    return player, (xBlocks*BLOCK_SIZE, yBlocks*BLOCK_SIZE)


def won():
    displayCentreText("You won!")
    pygame.time.wait(2000)
    quit()


def lost():
    displayCentreText("Game over!")
    pygame.time.wait(2000)
    quit()


def displayCentreText(text):
    window.fill(BACKGROUND_COLOUR)
    label = loadingFont.render(text, 1, (255, 255, 255))
    labelWidth = label.get_width()
    labelHeight = label.get_height()
    window.blit(label, (windowSize[0]/2-labelWidth/2, windowSize[1]/2-labelHeight/2)) # Centre the label
    pygame.display.flip()


# Sprite groups
wall_list = pygame.sprite.Group()
electrifiedWall_list = pygame.sprite.Group()
softspawn_list = pygame.sprite.Group()
spawnpoint_list = pygame.sprite.Group()
exitpoint_list = pygame.sprite.Group()
player_list = pygame.sprite.Group()
enemy_list = pygame.sprite.Group()
checkpoint_list = pygame.sprite.Group()

currentLevel = 1

# Set default window size
windowSize = (40*BLOCK_SIZE, 25*BLOCK_SIZE)

# Initialise the window
pygame.init()
os.environ["SDL_VIDEO_CENTERED"] = "1"
window = pygame.display.set_mode(windowSize)
pygame.display.set_caption("Maze Game")
pygame.mouse.set_visible(False)

# Define fonts
fpsFont = pygame.font.SysFont("", 30)
loadingFont = pygame.font.SysFont("", 70)
checkpointFont = pygame.font.SysFont("", 70)

# Display loading text
displayCentreText("Loading...")
pygame.display.flip()

# Block images
IMAGE_WALL = pygame.image.load("sprites/wall.png").convert()
IMAGE_ELECTRIFIED_WALL = pygame.image.load("sprites/electrifiedwall.png").convert()
IMAGE_SPAWNPOINT = pygame.image.load("sprites/spawnpoint.png").convert()
IMAGE_EXITPOINT = pygame.image.load("sprites/exitpoint.png").convert()
IMAGE_ENEMY = pygame.image.load("sprites/enemy.png").convert()
IMAGE_CHECKPOINT = pygame.image.load("sprites/checkpoint.png").convert()
IMAGE_PLAYER = pygame.image.load("sprites/player.png").convert()

# Load the first level and adjust the window size before going into the main loop
player, windowSize = loadLevel(currentLevel)
window = pygame.display.set_mode(windowSize)

# Timers
milliseconds = 0
seconds = 0

# Set the clock
clock = pygame.time.Clock()

playing = True

# MAIN LOOP
while True:
    if player.lives > 0:
        # If the player has won, store their score, go to the next level,
        # or finish the game if they have completed the last level.
        # Otherwise, carry on with the main loop
        if player.won:
            # Create the highscores directory if it doesn't exist
            filePath = "highscores/" + str(currentLevel)
            os.makedirs(os.path.dirname(filePath), exist_ok=True)
            
            # Get the current high score if it exists
            currentHighScore = 0
            if os.path.exists(filePath):
                with open(filePath, "r") as f:
                    currentHighScore = f.read().strip()
                    if currentHighScore:
                        currentHighScore = int(currentHighScore)
                    else:
                        currentHighScore = 0

            # Store the new score if it is higher than the current high score, or if there is no current high score
            score = int((player.lives/seconds)*10000)
            if score > currentHighScore:
                with open(filePath, 'w+') as f:
                    f.write(str(score))
                displayCentreText("You set a new high score! (" + str(score) + ")")
                pygame.time.wait(2000)
            
            # Reset the timers
            milliseconds = 0
            seconds = 0
            
            # Move to the next level, or end the game if the last level has been played
            currentLevel += 1
            if currentLevel <= LEVEL_COUNT:
                player, windowSize = loadLevel(currentLevel)
                window = pygame.display.set_mode(windowSize)
            else:
                won()
        else:
            # Delta time (change in time)
            dt = clock.tick(60)
            milliseconds += dt
            seconds = round(milliseconds/1000, 1)
            
            # Check for quit event
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
            
            # Clear screen
            window.fill(BACKGROUND_COLOUR)
            
            # Draw and update all sprites, leaving enemies and player until last so they are on top of all other sprites
            wall_list.draw(window)
            electrifiedWall_list.draw(window)
            softspawn_list.draw(window)
            spawnpoint_list.draw(window)
            exitpoint_list.draw(window)
            checkpoint_list.draw(window)
            enemy_list.draw(window)
            player_list.draw(window)
            
            wall_list.update()
            electrifiedWall_list.update()
            softspawn_list.update()
            spawnpoint_list.update()
            exitpoint_list.update()
            checkpoint_list.update()
            enemy_list.update(dt)
            player_list.update(dt)
            
            # Player lives remaining
            livesLabel = fpsFont.render("Lives: " + str(player.lives), 1, (255,255,0))
            window.blit(livesLabel, (5, 5))
            
            # Timer
            timeLabel = fpsFont.render("Time: " + str(seconds), 1, (255,255,0))
            window.blit(timeLabel, (5, 25))

            # FPS clock (uncomment when testing)
            #fpsLabel = fpsFont.render("FPS: " + str(int(clock.get_fps())), 1, (255,255,0))
            #window.blit(fpsLabel, (5, 45))
            
            # Update the display
            pygame.display.flip()
    else:
        # No more lives, the player has lost the game
        lost()
